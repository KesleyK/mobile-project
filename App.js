import React, { Component } from 'react';
import StatusBarIos from './src/components/StatusBarIOS';
import MainHeader from './src/components/MainHeader';
import Routes from './src/routes';

class App extends Component {
  render() {
    return (
      <React.Fragment>
        <StatusBarIos />
        <MainHeader />
        <Routes />
      </React.Fragment>
    );
  }
}

export default App;
