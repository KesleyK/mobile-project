import React from 'react';
import { Platform, View } from 'react-native';

import styles from './styles';

export default function StatusBarIOS(props) {
  return Platform.OS === 'ios' ? <View style={styles.Root} /> : null;
}
