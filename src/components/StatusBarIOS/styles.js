import { StyleSheet } from 'react-native';
import { responsiveHeight as rh } from '../../util/dimensions';

export default StyleSheet.create({
  Root: {
    color: '#eee',
    height: rh('4.5%'),
  },
});
