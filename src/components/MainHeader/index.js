import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

export default function MainHeader() {
  return (
    <View style={styles.Container}>
      <Text style={styles.Title}>Portal da COVID-19</Text>
    </View>
  );
}
