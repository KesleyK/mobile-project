import { StyleSheet } from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
} from '../../util/dimensions';

export default StyleSheet.create({
  Container: {
    backgroundColor: '#282828',
    paddingVertical: rh('1.5%'),
    borderBottomWidth: 1,
    borderColor: '#eee',
  },
  Title: {
    fontSize: rw('5%'),
    color: '#fff',
    textAlign: 'center',
  },
});
