import React from 'react';
import { TouchableOpacity, View, Image, Text, Linking } from 'react-native';

import styles from './styles';

export default function NewsCard({ item }) {
  const onPressedCardHandler = () => {
    Linking.openURL(item.link_url);
  };

  return (
    <TouchableOpacity onPress={onPressedCardHandler}>
      <View style={styles.Container}>
        <Image
          source={{
            uri: item.image_url,
          }}
          style={styles.Image}
        />
        <Text style={styles.Text}>{item.title}</Text>
      </View>
    </TouchableOpacity>
  );
}
