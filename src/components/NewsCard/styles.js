import { StyleSheet } from 'react-native';
import {
  responsiveHeight as rh,
  responsiveWidth as rw,
} from '../../util/dimensions';

export default StyleSheet.create({
  Container: {
    marginBottom: rh('4%'),
  },
  Image: {
    height: rh('25%'),
    width: '100%',
  },
  Text: {
    textAlign: 'center',
    fontSize: rw('4%'),
    color: '#eee',
  },
});
