import { StyleSheet } from 'react-native';

import {
  responsiveHeight as rh,
  responsiveWidth as rw,
} from '../util/dimensions';

export default StyleSheet.create({
  Icon: {
    color: '#eee',
    fontSize: rw('5.5%'),
  },
  TopBarStyle: {
    backgroundColor: '#444444',
  },
  BottomBarStyle: {
    backgroundColor: '#282828',
    borderTopColor: '#eee',
    borderWidth: 3,
  },
  IndicatorStyle: {
    height: 3.5,
    backgroundColor: '#2D80BB',
  },
});
