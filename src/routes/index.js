import React from 'react';
import screens from '../screens';
import Icon from 'react-native-vector-icons/dist/FontAwesome';
import { createAppContainer } from 'react-navigation';
import { createMaterialBottomTabNavigator } from 'react-navigation-material-bottom-tabs';
import { createMaterialTopTabNavigator } from 'react-navigation-tabs';

import styles from './styles';

const Home = createMaterialTopTabNavigator(
  {
    HomeNews: {
      screen: screens.HomeNews,
      navigationOptions: {
        title: 'Notícias',
      },
    },
    HomeAbout: {
      screen: screens.HomeAbout,
      navigationOptions: {
        title: 'Sobre',
      },
    },
  },
  {
    tabBarOptions: {
      indicatorStyle: styles.IndicatorStyle,
      style: styles.TopBarStyle,
    },
  },
);

const bottomTabs = createMaterialBottomTabNavigator(
  {
    HomeNews: { screen: Home },
    Profile: { screen: screens.Profile },
    Forum: { screen: screens.Forum },
  },
  {
    defaultNavigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, horizontal, tintColor }) => {
        const { routeName } = navigation.state;
        let TabIcon;

        switch (routeName) {
          case 'HomeNews':
            TabIcon = <Icon name="home" style={styles.Icon} />;
            break;
          case 'Profile':
            TabIcon = <Icon name="user" style={styles.Icon} />;
            break;
          case 'Forum':
            TabIcon = <Icon name="comment" style={styles.Icon} />;
            break;
        }

        return TabIcon;
      },
    }),
    labeled: false,
    barStyle: styles.BottomBarStyle,
  },
);

export default createAppContainer(bottomTabs);
