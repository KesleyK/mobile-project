import { StyleSheet } from 'react-native';

import {
  responsiveHeight as rh,
  responsiveWidth as rw,
} from '../../util/dimensions';

export default StyleSheet.create({
  Container: {
    flex: 1,
    backgroundColor: '#282828',
    padding: rw('3%'),
  },
  Title: {
    color: '#2D80BB',
    fontSize: rw('5%'),
    textAlign: 'center',
    marginBottom: rh('1.5%'),
  },
  MessagesContainer: {
    height: rh('60%'),
    backgroundColor: 'green',
    marginBottom: rh('3%'),
    backgroundColor: '#444444',
    borderRadius: 10,
  },
  MessageScrollView: {
    paddingHorizontal: rw('3.5%'),
  },
  Message: {
    marginVertical: rh('1%'),
  },
  RightMessage: {
    alignSelf: 'flex-end',
  },
  MessageBox: {
    backgroundColor: '#eee',
    width: '80%',
    paddingVertical: rh('2%'),
    paddingHorizontal: rw('4%'),
    borderRadius: 5,
  },
  MessageUsername: {
    color: '#eee',
    marginBottom: rh('.3%'),
  },
  InputHolder: {
    flexDirection: 'row',
    alignSelf: 'center',
  },
  Input: {
    flex: 1,
    backgroundColor: '#ccc',
    color: '#000',
    paddingVertical: rh('1%'),
    paddingHorizontal: rw('3%'),
    fontSize: rw('4%'),
    borderRadius: 3,
  },
  IconContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  Icon: {
    fontSize: rw('6%'),
    marginLeft: rw('2%'),
    color: '#ccc',
  },
});
