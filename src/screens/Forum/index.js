import React, { Component } from 'react';
import {
  TouchableOpacity,
  View,
  Text,
  ScrollView,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import styles from './styles';

class Forum extends Component {
  state = {
    ownMessages: [],
    messageInput: '',
  };

  onChangedInputHandler = (text) => {
    this.setState({ messageInput: text });
  };

  onAddedMessageHandler = () => {
    const ownMessages = [...this.state.ownMessages];
    ownMessages.push(this.state.messageInput);

    this.setState({ ownMessages, messageInput: '' });

    setTimeout(() => {
      this.scrollView.scrollToEnd({ animated: true });
    }, 50);
  };

  render() {
    const { ownMessages, messageInput } = { ...this.state };

    const renderedOwnMessages = ownMessages.map((m, index) => {
      return (
        <View key={index} style={[styles.Message, styles.RightMessage]}>
          <Text style={[styles.MessageUsername, styles.RightMessage]}>
            Você
          </Text>
          <View style={styles.MessageBox}>
            <Text>{m}</Text>
          </View>
        </View>
      );
    });

    const renderedFakeMessages = (
      <React.Fragment>
        <View style={styles.Message}>
          <Text style={styles.MessageUsername}>João</Text>
          <View style={styles.MessageBox}>
            <Text>Estou com febre. Devo me preocupar?!</Text>
          </View>
        </View>
        <View style={styles.Message}>
          <Text style={styles.MessageUsername}>Professora Ana</Text>
          <View style={styles.MessageBox}>
            <Text>
              Sim, João. Febre é um dos sintomas da COVID-19. Sentiu algo mais?
            </Text>
          </View>
        </View>
      </React.Fragment>
    );

    return (
      <View style={styles.Container}>
        <ScrollView keyboardShouldPersistTaps="handled">
          <Text style={styles.Title}>Fórum de Dúvidas</Text>
          <View style={styles.MessagesContainer}>
            <ScrollView
              style={styles.MessageScrollView}
              ref={(view) => {
                this.scrollView = view;
              }}>
              {renderedFakeMessages}
              {renderedOwnMessages}
            </ScrollView>
          </View>
          <View style={styles.InputHolder}>
            <TextInput
              placeholder="Digite sua mensagem"
              placeholderTextColor="#717171"
              style={styles.Input}
              value={messageInput}
              onChangeText={this.onChangedInputHandler}
            />
            <TouchableOpacity
              style={styles.IconContainer}
              onPress={this.onAddedMessageHandler}>
              <Icon name="send-o" style={styles.Icon} />
            </TouchableOpacity>
          </View>
        </ScrollView>
      </View>
    );
  }
}

export default Forum;
