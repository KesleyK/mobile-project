import Forum from './Forum';
import HomeAbout from './Home/About';
import HomeNews from './Home/News';
import Profile from './Profile';

export default {
  Forum,
  HomeAbout,
  HomeNews,
  Profile,
};
