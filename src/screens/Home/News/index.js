import React, { Component } from 'react';
import { ScrollView, Text, View, Image } from 'react-native';
import Card from '../../../components/NewsCard';
import styles from './styles';

import news from './metadata';

class News extends Component {
  render() {
    return (
      <ScrollView style={styles.ScrollView}>
        <View style={styles.Container}>
          {news.map((item, index) => (
            <Card key={index} item={item} />
          ))}
        </View>
      </ScrollView>
    );
  }
}

export default News;
