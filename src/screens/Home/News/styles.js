import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  ScrollView: {
    backgroundColor: '#282828',
  },
  Container: {
    padding: 10,
  },
});
