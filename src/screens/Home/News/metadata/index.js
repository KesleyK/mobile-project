export default [
  {
    title:
      'UE reserva 200 milhões de doses da potencial vacina contra Covid-19 da Biontech-Pfizer',
    image_url:
      'https://s2.glbimg.com/zqI4YXCp0AL22aHvwSNwZnM8quc=/0x0:3000x2000/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2020/V/e/EWuqt2T5eZ1YXPHw0jJQ/mpp20200721024.jpg',
    link_url:
      'https://g1.globo.com/bemestar/vacina/noticia/2020/09/09/ue-reserva-200-milhoes-de-doses-da-potencial-vacina-contra-covid-19-da-biontech-pfizer.ghtml',
  },
  {
    title: 'Mais de 148 mil maranhenses já se recuperaram da Covid-19',
    image_url:
      'https://s2.glbimg.com/-bF6tQWw7Nygnn2V6-Hxizvy_6M=/0x0:1300x867/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2020/g/0/8jj2AsTRSQeuTppC7MlA/28365-98dc8415-9dc4-52cd-b50d-bce2cb987292.jpg',
    link_url:
      'https://g1.globo.com/ma/maranhao/noticia/2020/09/08/mais-de-148-mil-maranhenses-ja-se-recuperaram-da-covid-19.ghtml',
  },
  {
    title:
      'Covid persistente: os sintomas e as sequelas mais comuns e que duram semanas, segundo 60 mil pacientes',
    image_url:
      'https://s2.glbimg.com/kDz3IGQUUAUAx-IWnGNzxAsOyLU=/0x0:660x371/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2020/V/2/A3N2zQRA29GFWkCEVMAg/covidpersistente.jpg',
    link_url:
      'https://g1.globo.com/bemestar/coronavirus/noticia/2020/09/10/covid-persistente-os-sintomas-e-as-sequelas-mais-comuns-e-que-duram-semanas-segundo-60-mil-pacientes.ghtml',
  },
  {
    title:
      '"Janeiro do ano que vem, a gente começa a vacinar todo mundo" contra a Covid, diz Pazuello',
    image_url:
      'https://s2.glbimg.com/bpjB16vNPh2xBbZ0zaeck5SPyoc=/0x0:4896x3264/1000x0/smart/filters:strip_icc()/i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2020/W/J/pwxQKaTBiOy8tdkCOTkQ/odr20200810008.jpg',
    link_url:
      'https://g1.globo.com/bemestar/coronavirus/noticia/2020/09/08/janeiro-do-ano-que-vem-a-gente-comeca-a-vacinar-todo-mundo-diz-pazuello.ghtml',
  },
  {
    title:
      'Voluntária que teve reação após vacina da AstraZeneca contra a Covid-19 não tomou placebo',
    image_url:
      'https://s2.glbimg.com/wYM3-Fz7o1RZYd2Gu66ohpUEvH8=/0x367:3313x2231/1080x608/smart/filters:max_age(3600)/https://i.s3.glbimg.com/v1/AUTH_59edd422c0c84a879bd37670ae4f538a/internal_photos/bs/2020/q/U/reQ5EEQfO0lzgmSf1aDg/000-1wg80x.jpg',
    link_url:
      'https://g1.globo.com/bemestar/vacina/noticia/2020/09/10/voluntaria-que-teve-reacao-apos-vacina-da-astrazeneca-e-uma-mulher-e-nao-tomou-placebo.ghtml',
  },
];
