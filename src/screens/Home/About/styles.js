import { StyleSheet } from 'react-native';

import {
  responsiveWidth as rw,
  responsiveHeight as rh,
} from '../../../util/dimensions';

export default StyleSheet.create({
  ScrollView: {
    backgroundColor: '#282828',
  },
  Container: {
    padding: rw('3%'),
  },
  SubContainer: {
    marginBottom: rh('3%'),
  },
  Title: {
    backgroundColor: '#eee',
    textAlign: 'center',
    paddingVertical: rh('2%'),
    fontSize: rw('3.8%'),
    fontWeight: 'bold',
    textTransform: 'uppercase',
    marginBottom: rh('2%'),
  },
  Item: {
    color: '#eee',
    fontSize: rw('4%'),
    marginBottom: rh('.5%'),
  },
  Paragraph: {
    color: '#eee',
    fontSize: rw('4%'),
    marginBottom: rh('2%'),
  },
});
