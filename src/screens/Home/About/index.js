import React, { Component } from 'react';
import { View, Text, ScrollView } from 'react-native';
import { symptoms, transmissions } from './metadata';

import styles from './styles';

class About extends Component {
  render() {
    const renderedSymptoms = symptoms.map((item, i) => {
      return (
        <Text key={i} style={styles.Item}>
          &bull;{item}
        </Text>
      );
    });

    const renderedTransmissions = transmissions.map((item, i) => {
      return (
        <Text key={i} style={styles.Item}>
          &bull;{item}
        </Text>
      );
    });

    return (
      <ScrollView style={styles.ScrollView}>
        <View style={styles.Container}>
          <View style={styles.SubContainer}>
            <Text style={styles.Title}>Sintomas</Text>
            <View>{renderedSymptoms}</View>
          </View>
          <View style={styles.SubContainer}>
            <Text style={styles.Title}>Como é transmititdo?</Text>
            <Text style={styles.Paragraph}>
              A transmissão acontece de uma pessoa doente para outra ou por
              contato próximo por meio de:
            </Text>
            <View>{renderedTransmissions}</View>
          </View>
          <View>
            <Text style={styles.Title}>Fonte</Text>
            <Text style={styles.Paragraph}>
              https://coronavirus.saude.gov.br/sobre-a-doenca
            </Text>
          </View>
        </View>
      </ScrollView>
    );
  }
}

export default About;
