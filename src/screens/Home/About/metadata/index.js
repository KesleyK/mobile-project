export const symptoms = [
  'Tosse',
  'Febre',
  'Coriza',
  'Dor de garganta',
  'Dificuldade para respirar',
  'Perda de olfato (anosmia)',
  'Alteração do paladar (ageusia)',
  'Distúrbios gastrintestinais (náuseas/vômitos/diarreia)',
  'Cansaço (astenia)',
  'Diminuição do apetite (hiporexia)',
  'Dispnéia ( falta de ar)',
];

export const transmissions = [
  'Toque do aperto de mão contaminadas;',
  'Gotículas de saliva;',
  'Espirro;',
  'Tosse;',
  'Catarro;',
  'Objetos ou superfícies contaminadas, como celulares, mesas, talheres, maçanetas, brinquedos, teclados de computador etc.',
];
