import React, { Component } from 'react';
import {
  Alert,
  ScrollView,
  Text,
  View,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/dist/FontAwesome';

import styles from './styles';

class Profile extends Component {
  state = {
    inputValue: '',
  };

  onChangedInputHandler = (text) => {
    this.setState({ inputValue: text });
  };

  onPressedButtonHandler = () => {
    Alert.alert(
      'Os dados do aluno foram atualizados!',
      'Agora você está pronto para entrar em nosso fórum de dúvidas.',
      [
        {
          text: 'Confirmar',
          onPress: () => this.setState({ inputValue: '' }),
        },
      ],
    );
  };

  render() {
    return (
      <ScrollView style={styles.ScrollView}>
        <View style={styles.Container}>
          <Text style={styles.Title}>Perfil do Aluno</Text>
          <Icon name="user-circle-o" size={33} style={styles.Icon} />
          <TextInput
            placeholder="Nome do aluno"
            value={this.state.inputValue}
            style={styles.Input}
            onChangeText={this.onChangedInputHandler}
          />
          <TouchableOpacity
            style={styles.Button}
            onPress={this.onPressedButtonHandler}>
            <Text style={styles.ButtonText}>Salvar</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

export default Profile;
