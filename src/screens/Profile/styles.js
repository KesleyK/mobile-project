import { StyleSheet } from 'react-native';

import {
  responsiveHeight as rh,
  responsiveWidth as rw,
} from '../../util/dimensions';

export default StyleSheet.create({
  ScrollView: {
    backgroundColor: '#282828',
  },
  Container: {
    flex: 1,
    padding: rw('3%'),
    alignItems: 'center',
  },
  Title: {
    color: '#2D80BB',
    fontSize: rw('5%'),
    textAlign: 'center',
    marginBottom: rh('20%'),
  },
  Icon: {
    color: '#eee',
    fontSize: rw('30%'),
    marginBottom: rh('5%'),
  },
  Input: {
    backgroundColor: '#ccc',
    color: '#000',
    paddingVertical: rh('1%'),
    paddingHorizontal: rw('3%'),
    fontSize: rw('4%'),
    borderRadius: 3,
    width: '80%',
    marginBottom: rh('4%'),
  },
  Button: {
    backgroundColor: '#2D80BB',
    width: '60%',
    paddingVertical: rh('1.3%'),
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowOpacity: 0.22,
    shadowRadius: 2.22,

    elevation: 3,
    borderRadius: 4,
  },
  ButtonText: {
    color: '#fff',
    fontWeight: 'bold',
    textTransform: 'uppercase',
  },
});
